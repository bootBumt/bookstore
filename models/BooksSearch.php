<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BooksSearch represents the model behind the search form about `app\models\Books`.
 */
class BooksSearch extends Books
{
    public $book_ids = [];
    public $author_ids = [];
    public $shop_ids = [];
    public $currentShop;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','publish_year'], 'integer'],
            [['book_ids', 'author_ids', 'shop_ids' ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = books::find();
        $query->joinwith(['authors', 'shops']);
        $dataProvider = new activedataprovider([
            'query' => $query->distinct(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['in', 'books.id', $this->book_ids]);
        $query->andFilterWhere(['in', 'authors.id', $this->author_ids]);
        $query->andFilterWhere(['in', 'shops.id', $this->shop_ids]);

        return $dataProvider;
    }
}
