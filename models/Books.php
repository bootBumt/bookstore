<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $title
 * @property string $released
 *
 * @property BooksAuthors[] $booksAuthors
 * @property BooksShops[] $booksShops
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'released'], 'required'],
            [['released'], 'safe'],
            [['title'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'released' => 'Released',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooksAuthors()
    {
        return $this->hasMany(BooksAuthors::className(), ['book_id' => 'id']);
    }
	
    public function getAuthors()
    {
        return $this->hasMany(Authors::className(), ['id' => 'author_id'])
            ->viaTable('books_authors', ['book_id' => 'id']);
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooksShops()
    {
        return $this->hasMany(BooksShops::className(), ['book_id' => 'id']);
    }
	
	    public function getShops()
    {
        return $this->hasMany(Shops::className(), ['id' => 'shop_id'])
            ->viaTable('books_shops', ['book_id' => 'id']);
    }
}
