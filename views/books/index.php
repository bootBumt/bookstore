<?php
use app\models\Books;
?>
    <p><b>Информация о книге</b></p>
    <p>
        <b>Название:</b>
        <?= $model->title ?>
    </p>
    <p><b>Автор: </b>
    <?php foreach ($model->authors as $author) {
        echo  $author->name . '; ';
    }?>
    </p>
    <p>
        <b>Год выпуска: </b>
        <?= $model->released ?>
    </p>
    <p><b>Магазин:</b>
    <?php foreach ($model->shops as $shop) {
        echo  $shop->title . '; ';
    }?>
    </p>
